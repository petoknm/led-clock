# Reverse Engineering the Hardware

![pcb](./images/debug_wiring.jpg)

## Microcontroller

- Part number: STC15F2K16S2
- Architecture: 8051 (Intel MCS-51)
- Maximum frequency: 28MHz
- Flash: 16kB
- SRAM: 2kB
- Package: LQFP44

[Datasheet](http://stcmicro.com/datasheet/STC15F2K60S2-en.pdf)

## LED Matrix

### Pinout (bottom view)

- A0..7 are anodes
- C0..7 are cathodes

![pinout](./images/led_matrix_pinout.jpg)

### Connections

All three displays share an 8 bit anode bus. The cathodes are all individual.
Cathodes of display 1 are `C1_[0..7]`, for display 2 it's `C2_[0..7]` and for
display 3 it's `C3_[0..7]`.

| LED Matrix | Microcontroller |
| ---------- | --------------- |
| A[0..7]    | P0.[0..7]       |
| C1_[0..7]  | P2.[0..7]       |
| C2_[0..3]  | P3.[4..7]       |
| C2_[4..7]  | P4.[1..4]       |
| C3_[0..1]  | P1.[0..1]       |
| C3_2       | P4.7            |
| C3_[3..4]  | P1.[2..3]       |
| C3_5       | P3.2            |
| C3_[6..7]  | P4.[5..6]       |

## RTC

The chip is DS1302(ZN). All three data signals have external 10k pull-ups.

| DS1302 | Microcontroller |
| ------ | --------------- |
| RST    | P4.0            |
| SCLK   | P5.5            |
| I/O    | P1.5            |

## Buttons

Two normally open push-buttons S1 & S2 are connected to pins P3.1 & P3.3
respectively. Other side of the push-buttons is grounded. No pull-ups observed.

## Thermistor

An NTC thermistor is attached to pin P1.6 with a 10k pull-up. Resistance of the
thermistor is around 5.7k.

## LDR

An NLC LDR is attached to pin P1.7 with a 10k pull-up. Resistance in the range
of 400R - 10k.

## Buzzer (Not confirmed)

Switched by a BJT connected to P5.4.
