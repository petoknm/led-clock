# 2019-04-07

Today, I reverse engineered the LED matrix pinout and its connections to the
MCU. I attached wires to the UART1 port for flashing but my CP2102 USB to UART
converter broke :'(. It's not showing up in `lsusb`. It seems, I will have to
wait for new CP2102s to arrive from China, because I can't find any other UART
converters.

```
[11042.922540] usb 1-6.1.4: new full-speed USB device number 29 using xhci_hcd
[11042.995885] usb 1-6.1.4: device descriptor read/64, error -32
[11043.175883] usb 1-6.1.4: device descriptor read/64, error -32
[11043.355874] usb 1-6.1.4: new full-speed USB device number 30 using xhci_hcd
[11043.429210] usb 1-6.1.4: device descriptor read/64, error -32
[11043.609206] usb 1-6.1.4: device descriptor read/64, error -32
[11043.715925] usb 1-6.1-port4: attempt power cycle
[11044.309181] usb 1-6.1.4: new full-speed USB device number 31 using xhci_hcd
[11044.309304] usb 1-6.1.4: Device not responding to setup address.
[11044.515952] usb 1-6.1.4: Device not responding to setup address.
[11044.722512] usb 1-6.1.4: device not accepting address 31, error -71
[11044.795833] usb 1-6.1.4: new full-speed USB device number 32 using xhci_hcd
[11044.795959] usb 1-6.1.4: Device not responding to setup address.
[11045.002613] usb 1-6.1.4: Device not responding to setup address.
[11045.209158] usb 1-6.1.4: device not accepting address 32, error -71
[11045.209293] usb 1-6.1-port4: unable to enumerate USB device
```

After I said goodbye to my USB to UART bridge, I started getting the basic
firmware infrastructure ready. A Makefile and a really basic firmware that
should just light up all the pixels. Unfortunately, that's probably all I can do
for now. Once I get some UART bridges I can flash it to the MCU and test if it
works.

# 2019-04-16

Some more reverse engineering. This time it was the push-buttons, RTC and
sensors. I am not sure about the buzzer. I will have to test it to confirm it. I
can't get a good view of the PCB now that it's soldered to track down the
connections around the buzzer. It was probably a good idea to take pictures of
the PCB before assembly.

# 2019-04-25

New CP2102 UART bridges arrived and I was finally able to flash the fw. I was
amazed when I saw it work on the first try. Now, that we know it works as
expected, we can continue to work on the firmware.

![first_fw](./images/first_fw.jpg)

A little bit of on-time calibration. More calibration will be done once we can
put arbitrary content on the screen.

![before_calibration](./images/before_calibration.jpg)
![after_calibration](./images/after_calibration.jpg)

# 2019-04-26

Today I implemented first UART RX functionality and COBS decoding, which I
pretty much just copied from Wikipedia. Here come our first frames sent over
UART.

```
stty -F /dev/ttyUSB0 9600 cs8 -cstopb -parenb
echo -ne '\x1b\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xaa\x55\xff\xff\x00' > /dev/ttyUSB0
```

![first_uart_frames](./images/first_uart_frames.jpg)

video:

[![first_uart_frames_video](http://img.youtube.com/vi/OBfbUaypWqA/0.jpg)](http://www.youtube.com/watch?v=OBfbUaypWqA)

# 2021-12-19

I just had a cool idea, let's create a super basic pixel shader VM and python
transpiler for it. This will allow us to upload pixel shaders over UART, that
can then hopefully redraw the entire screen at 60fps.

The VM will have access to some basic drawing API and inputs like frame number,
RTC time, LDR brightness, etc.
