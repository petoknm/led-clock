# led-clock

Reverse engineering a Chinese LED clock to build libre firmware for it.

- [HW Documentation](./HW.md)
- [Project Log](./LOG.md)

![clock](./images/clock.jpg)

## Dependencies

- [make](https://www.gnu.org/software/make/)
- [sdcc](http://sdcc.sourceforge.net/)
- [stcgal](https://github.com/grigorig/stcgal),
  [aur](https://aur.archlinux.org/packages/stcgal/)

## Roadmap

- [x] HW Reverse Engineering
- [x] Build and flash custom firmware
- [x] Interface with LED display
- [x] Interface with UART
- [ ] Interface with RTC
- [ ] Interface with LDR
- [ ] Define shader program VM
- [ ] Create shader program interpreter
- [ ] Hot-reload shader program over UART
