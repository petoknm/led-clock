#ifndef RTC_H
#define RTC_H

#include <stdint.h>
#include "stc15.h"

#define CE P4_0
#define SCLK P5_5
#define IO P1_5

void rtc_init();
uint32_t rtc_read();

#endif
