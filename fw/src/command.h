#ifndef COMMAND_H
#define COMMAND_H

#include <stdint.h>

typedef enum {
  COMMAND_SET_FB = 0,
  COMMAND_SET_SHADER = 1,
} command_type_t;

typedef struct {
  command_type_t type;
  uint8_t *data;
} command_t;

#endif // COMMAND_H