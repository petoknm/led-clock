#ifndef UART_H
#define UART_H

#include <stdint.h>

#define FOSC 14746000
#define BAUD 115200
#define PRESC (65536 - (FOSC / 4 / BAUD))

void uart_init();
void uart_poll();

#endif // UART_H