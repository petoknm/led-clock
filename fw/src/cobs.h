#ifndef COBS_H
#define COBS_H

#include <stdint.h>

uint8_t cobs_decode(const uint8_t *src, uint8_t len, uint8_t *dst);

#endif // COBS_H
