#ifndef DRAW_H
#define DRAW_H

#include <stdint.h>

void draw_char(uint8_t *buf, char c);
void draw_str(uint8_t *buf, char *str);
void draw_col(uint8_t *buf, uint8_t col, uint8_t val);
void draw_pix(uint8_t *buf, uint8_t col, uint8_t row, uint8_t val);

#endif // DRAW_H