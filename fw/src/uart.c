#include "uart.h"
#include "cobs.h"
#include "stc15.h"

typedef struct {
  uint8_t buf[32];
  uint8_t pos;
  uint8_t len;
  uint8_t ready;
} uart_rx_t;

static __xdata uart_rx_t uart_rx = {{0}, 0, 0, 0};

void uart_init() {
  SCON = 0x50;        // 8bit, variable baud rate
  T2L = PRESC & 0xFF; // Setup timer2 prescaler
  T2H = PRESC >> 8;
  AUXR = 0x14;  // timer2 in 1T mode and run
  AUXR |= 0x01; // select timer2 as uart1 baud rate generator
  ES = 1;       // enable serial interrupt
  EA = 1;       // enable global interrupts
}

void uart_poll() {
  if (uart_rx.ready) {
    uint8_t decode_buf[32];
    uint8_t decode_len = cobs_decode(uart_rx.buf, uart_rx.len, decode_buf);
    uart_rx.ready = 0;

    // 26 byte payload + zero byte
    // TODO check crc
    if (decode_len == 27) {
      // memcpy(led_data, decode_buf, 24);
    }
  }
}

void uart_isr() __interrupt(4) {
  if (RI) {
    RI = 0;
    uint8_t data = SBUF;
    if (uart_rx.len < 32 && !uart_rx.ready) {
      // fits in the buffer and is finished processing
      uart_rx.buf[uart_rx.pos++] = data;
    }
    if (data == 0) { // frame end
      uart_rx.len = uart_rx.pos;
      uart_rx.ready = 1;
      uart_rx.pos = 0;
    }
  }
}