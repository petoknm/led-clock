#include "ldr.h"
#include "stc15.h"

void ldr_init() {
    CLK_DIV |= 1 << 5; // adrj=1
    P1ASF = 0x80;
    ADC_CONTR = 0xC7; // power=on, speed=2, ch=7, start=0
}

uint16_t ldr_read() {
    ADC_RES = 0;
    ADC_RESL = 0;
    ADC_CONTR = 0xCF; // power=on, speed=2, ch=7, start=1
    while (!(ADC_CONTR & (1 << 4))); // wait for ADC_FLAG
    ADC_CONTR = 0xC7; // power=on, speed=2, ch=7, start=0
    return (ADC_RES << 8) | ADC_RESL;
}