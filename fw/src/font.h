#ifndef FONT_H
#define FONT_H

#include <stdint.h>

extern const uint8_t font[256][5];

#endif
