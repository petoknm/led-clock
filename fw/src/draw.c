#include "draw.h"
#include "font.h"
#include <string.h>

void draw_char(uint8_t *buf, char c) { memcpy(buf, font[c], 5); }

void draw_str(uint8_t *buf, char *str) {
  char c;
  while (c = *str++) {
    draw_char(buf, c);
    buf += 5;
  }
}

void draw_col(uint8_t *buf, uint8_t col, uint8_t val) { buf[col] = val; }

void draw_pix(uint8_t *buf, uint8_t col, uint8_t row, uint8_t val) {
  if (val)
    buf[col] |= 1 << row;
  else
    buf[col] &= ~(1 << row);
}