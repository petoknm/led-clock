#include "cobs.h"

uint8_t cobs_decode(const uint8_t *src, uint8_t len, uint8_t *dst) {
  const uint8_t *start = dst;
  const uint8_t *end = src + len;
  uint8_t code = 0xFF;
  uint8_t copy = 0;

  for (; src < end; copy--) {
    if (copy != 0) {
      *dst++ = *src++;
    } else {
      if (code != 0xFF)
        *dst++ = 0;
      copy = code = *src++;
      if (code == 0)
        break;
    }
  }

  return dst - start;
}
