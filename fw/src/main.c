#include <stdint.h>
#include <string.h>
#include "display.h"
#include "uart.h"
#include "ldr.h"
#include "rtc.h"

static void delay(uint16_t dt) {
  while (dt--)
    __asm__("nop");
}

void main() {
  display_init();
  uart_init();
  ldr_init();
  rtc_init();

  while (1) {
    uart_poll();
    // TODO: display polling
    // TODO: ldr polling
    // TODO: rtc polling
  }
}
