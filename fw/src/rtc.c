#include "rtc.h"

static void reset_3w() {
  SCLK = 0;
  CE = 0;
  CE = 1;
}

static void write_3w(uint8_t v) {
  for (uint8_t i = 0; i < 8; ++i) {
    if (v & 0x01) {
      IO = 1;
    } else {
      IO = 0;
    }
    SCLK = 0;
    SCLK = 1;
    v >>= 1;
  }
}

static uint8_t read_3w() {
  uint8_t v = 0x00, tmp;

  IO = 1;
  for (uint8_t i = 0; i < 8; i++) {
    SCLK = 1;
    SCLK = 0;
    tmp = (uint8_t)IO;
    tmp <<= 7;
    v >>= 1;
    v |= tmp;
  }
  return v;
}

/* Example code
void writebyte() //  --- write one byte using values entered by user ---
{
  uchar ClkAdd;
  uchar ClkData;

  //  Get Clock Address & Data
  printf("\nEnter register write address (hex): ");
  scanf("%bx", &ClkAdd);
  printf("Enter data (hex): ");
  scanf("%bx", &ClkData);

  reset_3w();
  wbyte_3w(ClkAdd);
  wbyte_3w(ClkData);
  reset_3w();
}

void read_clk_regs() //  ---- loop read & display clock registers ----
{
  uchar prev_sec = 99, sec, min, hrs, dte, mon, day, yr;

  printf("\nYr/Mn/Dt Dy Hr/Mn/Sec");
  do //  Read & Display Clock Registers
  {
    reset_3w();
    wbyte_3w(0xBF); //  clock burst
    sec = rbyte_3w();
    min = rbyte_3w();
    hrs = rbyte_3w();
    dte = rbyte_3w();
    mon = rbyte_3w();
    day = rbyte_3w();
    yr = rbyte_3w();
    reset_3w();
    if (sec != prev_sec) //  print once per second
    {
      printf("\n%02bX/%02bX/%02bX %02bX", yr, dte, mon, day);
      printf(" %02bX:%02bX:%02bX", hrs, min, sec);
      prev_sec = sec;
    }
  } while (!RI);
  RI = 0; //  clear read buffer
}

void burstramrd() //  ----------- read RAM in burst mode ---------------
{
  uchar i;

  printf("\nDS1302 Ram");

  reset_3w();
  wbyte_3w(0xFF); //  RAM burst read
  for (i = 0; i < 31; i++) {
    if (!(i % 8))
      printf("\n");
    printf("%2.bX ", rbyte_3w());
  }
  reset_3w();
}

void burstramwr() //  ---- write one value entire RAM in burst mode ----
{
  uchar ramdata;
  uchar i;

  printf("\nWrite Ram DATA (HEX):"); //  Get Ram Data
  scanf("%bx", &ramdata);

  reset_3w();
  wbyte_3w(0xfe); //  RAM burst write
  for (i = 0; i < 31; ++i) {
    wbyte_3w(ramdata);
  }
  reset_3w();
}

void write_clk_regs() //  --- initialize time & date from user entries ---
//  Note: NO error checking is done on the user entries!
{
  uchar yr, mn, date, dy, hr, min, sec, day;

  printf("\nEnter the year (0-99): ");
  scanf("%bx", &yr);
  printf("Enter the month (1-12): ");
  scanf("%bx", &mn);
  printf("Enter the date (1-31): ");
  scanf("%bx", &date);
  printf("Enter the day (1-7): ");
  scanf("%bx", &dy);
  printf("Enter the hour (1-24): ");
  scanf("%bx", &hr);
  hr = hr & 0x3f; //  force clock to 24 hour mode
  printf("Enter the minute (0-59): ");
  scanf("%bx", &min);
  printf("Enter the second (0-59): ");
  scanf("%bx", &sec);

  reset_3w();
  wbyte_3w(0x8e); //  control register
  wbyte_3w(0);    //  disable write protect
  reset_3w();
  wbyte_3w(0x90); //  trickle charger register
  wbyte_3w(0xab); //  enable, 2 diodes, 8K resistor
  reset_3w();
  wbyte_3w(0xbe); //  clock burst write (eight registers)
  wbyte_3w(sec);
  wbyte_3w(min);
  wbyte_3w(hr);
  wbyte_3w(date);
  wbyte_3w(mn);
  wbyte_3w(dy);
  wbyte_3w(yr);
  wbyte_3w(0); //  must write control register in burst mode
  reset_3w();
}
*/

void rtc_init() {}

uint32_t rtc_read() {
  return 0;
}