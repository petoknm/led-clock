#include "display.h"
#include "stc15.h"

static __xdata uint8_t buffer[2 * 24] = {0};
static uint8_t *front = buffer;
static uint8_t *back = buffer + 24;

static uint8_t col = 0;

void display_init() {
  // Configure P0 as push-pull
  P0M1 = 0x00;
  P0M0 = 0xff;
}

void display_swap_buffers() {
  uint8_t *t = front;
  front = back;
  back = t;
}

static void set_col(uint8_t col, uint8_t val) {
  // clang-format off
  switch (col) {
    case 0: P2_0 = val; break;
    case 1: P2_1 = val; break;
    case 2: P2_2 = val; break;
    case 3: P2_3 = val; break;
    case 4: P2_4 = val; break;
    case 5: P2_5 = val; break;
    case 6: P2_6 = val; break;
    case 7: P2_7 = val; break;
    case 8: P3_4 = val; break;
    case 9: P3_5 = val; break;
    case 10: P3_6 = val; break;
    case 11: P3_7 = val; break;
    case 12: P4_1 = val; break;
    case 13: P4_2 = val; break;
    case 14: P4_3 = val; break;
    case 15: P4_4 = val; break;
    case 16: P1_0 = val; break;
    case 17: P1_1 = val; break;
    case 18: P4_7 = val; break;
    case 19: P1_2 = val; break;
    case 20: P1_3 = val; break;
    case 21: P3_2 = val; break;
    case 22: P4_5 = val; break;
    case 23: P4_6 = val; break;
  }
  // clang-format on
}

void display_next_column() {
  set_col(col, 0);
  col = (col + 1) % 24;
  P0 = front[col];
  set_col(col, 1);
}