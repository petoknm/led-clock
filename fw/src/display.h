#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>

void display_init();
void display_swap_buffers();
void display_next_column();

#endif // DISPLAY_H