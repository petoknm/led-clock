#ifndef LDR_H
#define LDR_H

#include<stdint.h>

void ldr_init();
uint16_t ldr_read();

#endif // LDR_H